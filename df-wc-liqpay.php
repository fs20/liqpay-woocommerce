<?php
/**
Plugin Name: LiqPay for Woocommerce
Description: This plugin adds LiqPay payment method to the woocommerce
Version: 1.0
Author: Andrii Chaikovskyi
Author URI: http://dev-force.com
License: GPLv2
 */
define('DF_WC_LIQPAY_VERSION', '1.0.0');
spl_autoload_register('df_wc_liqpay_autoload');
function df_wc_liqpay_autoload($class) {
    $file_path = __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, strtolower(str_replace('DF_WC_LiqPay', '', $class)).'.php');
    if (file_exists($file_path)) {
        require_once($file_path);
    }
}

add_action('woocommerce_init', function(){
    $config = [
        'plugin_dir' => plugin_dir_path(__FILE__),
        'base_name' => plugin_basename(__FILE__),
    ];
    \DF_WC_LiqPay\App::instance()->run($config);
});