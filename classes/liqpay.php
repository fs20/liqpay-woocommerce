<?php
namespace DF_WC_LiqPay;

class LiqPay extends \WC_Payment_Gateway
{

    /**
     * Setting required values
     */
    public function __construct()
    {
        $this->id = 'df-wc-liqpay';
        $this->method_title = __('LiqPay', 'df-wc-liqpay');
        $this->method_description = __('This method allows you to accept payments using LiqPay', 'df-wc-liqpay');
        $this->init_form_fields();
        $this->init_settings();
        $this->title = $this->get_option('title');
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options'));
    }

    /**
     * Initializing form fields
     *
     * @return $this
     */
    public function init_form_fields()
    {
        $this->form_fields = [
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'df-wc-liqpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable LiqPay', 'df-wc-liqpay' ),
                'default' => 'no'
            ),
            'title' => array(
                'title' => __( 'Title', 'df-wc-liqpay' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'df-wc-liqpay' ),
                'default' => __( 'LiqPay', 'df-wc-liqpay' ),
                'desc_tip'      => true,
            ),
            'is_sandbox' => array(
                'title'       => __( 'LiqPay Sandbox enabled?', 'df-wc-liqpay' ),
                'type'        => 'checkbox',
                'label'       => __( 'LiqPay Sandbox enabled?', 'df-wc-liqpay' ),
                'default'     => 'no',
                'description' => __( 'LiqPay sandbox can be used to test payments.', 'df-wc-liqpay' ),
            ),
            'api_details' => array(
                'title'       => __( 'API credentials', 'df-wc-liqpay' ),
                'type'        => 'title',
                'description' => __( 'Enter your LiqPay API credentials', 'df-wc-liqpay' ),
            ),
            'public_key' => array(
                'title'       => __( 'Public Key', 'df-wc-liqpay' ),
                'type'        => 'text',
                'description' => __( 'Get your API credentials from LiqPay.', 'df-wc-liqpay' ),
                'default'     => '',
                'desc_tip'    => true,
                'placeholder' => __( 'Public Key', 'df-wc-liqpay' ),
            ),
            'private_key' => array(
                'title'       => __( 'Private Key', 'df-wc-liqpay' ),
                'type'        => 'password',
                'description' => __( 'Get your API credentials from LiqPay.', 'df-wc-liqpay' ),
                'default'     => '',
                'desc_tip'    => true,
                'placeholder' => __( 'Private Key', 'df-wc-liqpay' ),
            ),
            'processing_url' => array(
                'title'       => __( 'Processing Endpoint', 'df-wc-liqpay' ),
                'type'        => 'text',
                'description' => __( 'Processing url.', 'df-wc-liqpay' ),
                'default'     => '',
                'desc_tip'    => true,
                'placeholder' => __( 'processing-url', 'df-wc-liqpay' ),
            ),
            'language' => array(
                'title' => __('Language', 'df-wc-liqpay'),
                'type' => 'select',
                'default' => 'uk',
                'options'  => array(
                    'uk'        => __( 'Ukrainian', 'df-wc-liqpay' ),
                    'en' => __( 'English', 'df-wc-liqpay' ),
                    'ru'   => __( 'Russian', 'df-wc-liqpay' ),
                ),
            ),
        ];
        return $this;
    }

    /**
     * Processing a payment
     *
     * @param int $order_id
     * @return array
     */
    public function process_payment($order_id)
    {
        $order = new \WC_Order( $order_id);
        $order->update_status('on-hold', __( 'Awaiting cheque payment', 'df-wc-liqpay' ));
        $order->reduce_order_stock();
        return array(
            'result'   => 'success',
            'redirect' => '#liqpay-pay',
        );
    }
}