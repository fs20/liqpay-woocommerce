<?php
namespace DF_WC_LiqPay;
class Config extends \DF\Config
{
    protected $_required_params = [
        'plugin_dir',
        'base_name'
    ];

    protected static $_instance = null;

    /**
     * Get Instance
     *
     * @param string $app_code
     * @return Config|null
     */
    public static function get_instance($app_code = '')
    {
        if (!self::$_instance) {
            self::$_instance = new self((new \DF_WC_LiqPay\LiqPay())->get_option_key());
        }
        return self::$_instance;
    }

    /**
     * Loading configuration
     *
     * @param array $options
     * @throws \Exception
     */
    public function load($options)
    {
        $this->_options = array_merge($this->_options, $options);
        foreach ($this->_required_params as $param) {
            if (isset($this->_options[$param])) {
                continue;
            }
            throw new \Exception('Parameter '.$param.' is required');
        }
    }

    /**
     * Checking whether sandbox mode is enabled
     *
     * @return bool
     */
    public function is_sandbox()
    {
        return $this->get('is_sadbox') == 'yes';
    }
}