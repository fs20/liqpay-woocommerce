<?php
namespace DF_WC_LiqPay;

use DF\Request;

class App extends  \DF\App
{
    const CODE = 'df-wc-liqpay';

    protected static $_instance = null;

    /**
     * Configuring application settings
     *
     * @param array $options
     * @return $this
     */
    protected function _init($options)
    {
        $config = \DF_WC_LiqPay\Config::get_instance(self::CODE);
        $config->load($options);
        add_filter( 'woocommerce_payment_gateways', [$this, 'register_payment_method'] );
        add_action('wp_enqueue_scripts', [$this, 'register_assets']);

        add_action('wp_ajax_df_wc_liqpay_pay', [$this, 'get_order_data']);
        add_action('wp_ajax_nopriv_df_wc_liqpay_pay', [$this, 'get_order_data']);

        add_action('wp_ajax_df_wc_liqpay_process', [$this, 'process_callback']);
        add_action('wp_ajax_nopriv_df_wc_liqpay_process', [$this, 'process_callback']);
        return $this;
    }

    /**
     * Registering LiqPay payment method within woocommerce payment methods
     *
     * @param array $methods
     * @return mixed
     */
    public function register_payment_method($methods)
    {
        array_push($methods, new LiqPay());
        return $methods;
    }

    /**
     * Retrieving path inside plugins folder
     *
     * @param array|string $relativePath
     * @return string
     */
    public function get_file_path($relativePath)
    {
        if (is_array($relativePath)) {
            $relativePath = implode(DIRECTORY_SEPARATOR, $relativePath);
        }
        return $this->get_plugin_dir().DIRECTORY_SEPARATOR.$relativePath;
    }

    /**
     * Retrieving plugin directory
     *
     * @return mixed
     */
    public function get_plugin_dir()
    {
        return Config::get_instance()->get('plugin_dir');
    }

    /**
     * Retrieving plugin url
     *
     * @param $path string
     * @return string
     */
    public function get_url($path)
    {
        return plugins_url($path, Config::get_instance()->get('base_name'));
    }

    /**
     * Registering required assets
     */
    public function register_assets()
    {
        wp_register_script('df-wc-liqpay-lib', '//static.liqpay.com/libjs/checkout.js', ['jquery'], DF_WC_LIQPAY_VERSION, true);
        wp_register_script('df-wc-liqpay', $this->get_url('/assets/js/df-wc-liqpay.js'), ['jquery', 'df-wc-liqpay-lib'], DF_WC_LIQPAY_VERSION, true);
        wp_localize_script('df-wc-liqpay', 'DfWcLqp', ['ajax_url' => admin_url('admin-ajax.php')]);
        wp_enqueue_script('df-wc-liqpay-lib');
        wp_enqueue_script('df-wc-liqpay');
        return $this;
    }

    /**
     * Retrieving order data for ajax request
     */
    public function get_order_data()
    {
        if (!$order_id = WC()->session->get('order_awaiting_payment')) {
            wp_die(json_encode(['status' => 'FAILED', 'message' => __('Order not found', 'df-wc-liqpay')]));
        }

        $order = new \WC_Order($order_id);
        if (!$order->get_id()) {
            wp_die(json_encode(['status' => 'FAILED', 'message' => __('Order not found', 'df-wc-liqpay')]));
        }

        $api = new Api(Config::get_instance()->get('public_key'), Config::get_instance()->get('private_key'));
        $data = [
            'order_id' => $order->get_id(),
            'action' => 'pay',
            'description' => __(sprintf('Payment for order #%s', $order->get_id()), 'df-wc-liqpay'),
            'amount' => $order->get_total(),
            'sandbox' => Config::get_instance()->is_sandbox(),
            'language' => Config::get_instance()->get('language'),
            'currency' => $order->get_currency(),
            'server_url' => admin_url('admin-ajax?df_wc_liqpay_process'),
            'version' => $api->get_version(),
        ];
        $order->update_status('on-hold', __('Awaiting payment', 'df-wc-liqpay'));
        $signature = $api->cnb_signature($data);
        wp_die(json_encode([
            'status' => 'OK',
            'signature' => $signature,
            'data' => base64_encode(json_encode($api->cnb_params($data))),
        ]));
    }

    /**
     * Processing liqpay callback closing|
     */
    public function process_callback()
    {
        $request = Request::get_instance();
        if (!$request->server()->is('REQUEST_METHOD', 'POST')) {
            wp_die(json_encode(['status' => 'FAILURE', 'message' => __('Request method not supported', 'df-wc-liqpay')]));
        }
        // @todo compare signatures
        // @todo close order and send payment confirmation email
    }
}
