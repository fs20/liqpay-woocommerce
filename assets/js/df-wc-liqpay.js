window.addEventListener('hashchange', function(e){
    if (window.location.hash !== '#liqpay-pay') {
        return false;
    }
    window.LiqPayCheckoutCallback();
}, false);

window.LiqPayCheckoutCallback = function() {
    jQuery.ajax({
        url: DfWcLqp.ajax_url+'?action=df_wc_liqpay_pay',
        dataType: 'json',
        type: 'GET',
        success: liqpay_pay
    });
    function liqpay_pay(response) {
        if(response.status != 'OK') {
            return false;
        }
        console.log(response);
        LiqPayCheckout.init({
            data: response.data,
            signature: response.signature,
            // embedTo: "#liqpay_checkout",
            mode: "popup" // embed || popup
        }).on('liqpay.callback', function(data){
                console.log(data.status);
                console.log(data);
            }).on('liqpay.ready', function(data){
                // ready
            }).on('liqpay.close', function(data){
                // close
            });
    }
};